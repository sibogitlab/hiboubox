import logging
import time
import subprocess

from tempfile import NamedTemporaryFile
from threading import Thread, Lock
from base64 import b64decode

from odoo import http

import odoo.addons.hw_drivers.controllers.proxy as hw_proxy

_logger = logging.getLogger(__name__)

DRIVER_NAME = 'print_queue'


class PrintQueueThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.bashlock = Lock()
        self.status = {'status': 'connecting', 'messages': []}
        self.device = 'None'
        self.device_command = 'lpstat -p'

    def set_status(self, status, message=None):
        if status == self.status['status']:
            if message is not None and message != self.status['messages'][-1]:
                self.status['messages'].append(message)

                if status == 'error' and message:
                    _logger.error('Print Error: ' + message)
                elif status == 'disconnected' and message:
                    _logger.warning('Printer Disconnected: ' + message)
        else:
            self.status['status'] = status
            if message:
                self.status['messages'] = [message]
            else:
                self.status['messages'] = []

            if status == 'error' and message:
                _logger.error('Print Error: ' + message)
            elif status == 'disconnected' and message:
                _logger.info('Disconnected Printer: %s', message)

    def get_device(self):
        if self.device:
            return self.device

        process = subprocess.Popen(self.device_command.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()
        if error:
            _logger.warn('cannot get status of printing subsystem')
            return None
        else:
            lines = output.split('\n')
            printer_detail = []
            for line in lines:
                pieces = line.split(' ')
                if len(pieces) >= 4 and pieces[0] == 'printer':
                    printer_detail.append(pieces[1])
            return printer_detail

    def print_attachment(self, attachment):
        printer = attachment.get('printer')
        if not printer:
            printer = 'default'
        if attachment.get('filename', '').lower().find('.zpl') >= 0:
            raw = True
        else:
            raw = False
        with self.bashlock:
            with NamedTemporaryFile(delete=False) as f:
                f.write(b64decode(attachment['data']))
            cmd = 'lp -d ' + printer
            if raw:
                cmd += ' -o raw'
            cmd += ' ' + str(f.name)
            _logger.info('printing with full command: ' + cmd)
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            output, error = process.communicate()
            if error:
                _logger.warn('print_attachment error: ' + str(error))
        return True

    def get_status(self):
        return self.status

    def run(self):
        self.device = None
        self.set_status('connected')
        while True:
            time.sleep(10)


printer_thread = PrintQueueThread()
printer_thread.start()
hw_proxy.proxy_drivers[DRIVER_NAME] = printer_thread


class PrintQueueDriver(http.Controller):
    @http.route('/hw_proxy/print_queue', type='json', auth='none', cors='*')
    def print_queue(self, attachment):
        # {
        #     mimetype: "application/octet-stream",
        #     id: 5344,
        #     name: "LabelFedex-787623177789.ZPLII",
        #     filename: "LabelFedex-787623177789.ZPLII",
        #     url: "/web/content/5344?download=true",
        #     data: base64blob
        # }
        printer_thread.print_attachment(attachment)

    # @http.route('/hw_proxy/print_queue/list', type='json', auth='none', cors='*')
    # def print_queue_list(self):
    #     return printer_thread.device

