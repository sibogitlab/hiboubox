# Hibou Boxes

## Setup
1. Download and install **v21_10** (see `init.sh` for compatible releases) of 
   POSBox/IOTBox from **nightly.odoo.com**

2. *Optional* Download and prepare `init.sh` script (e.g. SSL certs or specific modules)

3. Run script on newly imaged/booted Raspberry Pi as root (`sudo -s` first) 
   (e.g. if you have no setup, `curl https://gitlab.com/hibou-io/hibou-odoo/hiboubox/-/raw/master/init.sh | bash`)

4. Start printing and shipping like a Hibou Professional!
