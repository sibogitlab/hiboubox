import logging
import time

from .pyusb_scale.scale import Scale
from threading import Thread, Lock

from odoo import http

import odoo.addons.hw_drivers.controllers.proxy as hw_proxy

# Instead, I'll just remove the driver
# from odoo.addons.hw_drivers.iot_handlers.drivers.SerialScaleDriver import ScaleReadOldRoute

_logger = logging.getLogger(__name__)

# Why not register as your own driver?
# POS looks at the status of the 'scale' driver
# instead of just trying to read from the scale
# if you have the scale enabled.
DRIVER_NAME = 'scale'


class ScaleThread(Thread):
    def __init__(self):
        super().__init__(daemon=True)
        self.lock = Lock()
        self.status = {'status': 'connecting', 'messages': []}
        self.weight = 0.0
        self.weight_info = 'ok'
        self.device = None

    def set_status(self, status, message=None):
        if status == self.status['status']:
            if message is not None and message != self.status['messages'][-1]:
                self.status['messages'].append(message)

                if status == 'error' and message:
                    _logger.error('Scale Error: '+ message)
                elif status == 'disconnected' and message:
                    _logger.warning('Disconnected Scale: '+ message)
        else:
            self.status['status'] = status
            if message:
                self.status['messages'] = [message]
            else:
                self.status['messages'] = []

            if status == 'error' and message:
                _logger.error('Scale Error: '+ message)
            elif status == 'disconnected' and message:
                _logger.info('Disconnected Scale: %s', message)

    def _parse_weight_answer(self, reading):
        """ Parse a scale's reading to a weighing request, returning
            a `(weight, weight_info, status)` pair.
        """
        weight, weight_info, status = None, None, None
        try:
            weight = reading.weight
            unit = reading.unit
            weight_info = reading.status

            if unit == 'kilogram':
                pass
            elif unit == 'pound':
                weight /= 2.20462
            elif unit == 'ounce':
                weight /= 35.274
            elif unit == 'gram':
                weight /= 1000.0
            else:
                raise Exception('Unsupported unit: %s' % (unit,))
        except Exception as e:
            _logger.exception("Cannot parse scale answer [%r] : %s", reading, e)
            status = ("Could not weigh on scale")
        return weight, weight_info, status

    def get_device(self):
        if self.device:
            return self.device

        return None

    def get_weight(self):
        return self.weight

    def get_weight_info(self):
        return self.weight_info

    def get_status(self):
        return self.status

    def read_weight(self):
        try:
            if self.device:
                reading = self.device.weigh()
                if reading:
                    weight, weight_info, status = self._parse_weight_answer(reading)
                    if status:
                        self.set_status('error', status)
                        self.device = None
                    else:
                        if weight is not None:
                            self.weight = weight
                        if weight_info is not None:
                            self.weight_info = weight_info
        except Exception as e:
            self.set_status(
                'error',
                "Could not weigh on scale " + str(e))
            self.device = None

    def set_zero(self):
        pass

    def set_tare(self):
        pass

    def clear_tare(self):
        pass

    def run(self):
        # self.device = None

        while True:
            try:
                if self.device:
                    old_weight = self.weight
                    self.read_weight()
                    if self.weight != old_weight:
                        _logger.info('New Weight: %s, sleeping %ss', self.weight, 0.5)
                        time.sleep(0.5)
                    else:
                        _logger.info('Weight: %s, sleeping %ss', self.weight, 0.5)
                        time.sleep(0.5)
                else:
                    with self.lock:
                        self.device = Scale()
                        if self.device:
                            self.set_status('connected')
                    if not self.device:
                        # retry later to support "plug and play"
                        time.sleep(2)
            except Exception as e:
                _logger.error('Exception in scale read: ' + str(e))


scale_thread = ScaleThread()
scale_thread.start()
hw_proxy.proxy_drivers[DRIVER_NAME] = scale_thread


class ScaleDriver(http.Controller):
    @http.route('/hw_proxy/scale_read/', type='json', auth='none', cors='*')
    def scale_read(self):
        return {'weight': scale_thread.get_weight(),
                'unit': 'kg',
                'info': scale_thread.get_weight_info()}

    @http.route('/hw_proxy/scale_zero/', type='json', auth='none', cors='*')
    def scale_zero(self):
        scale_thread.set_zero()
        return True

    @http.route('/hw_proxy/scale_tare/', type='json', auth='none', cors='*')
    def scale_tare(self):
        scale_thread.set_tare()
        return True

    @http.route('/hw_proxy/scale_clear_tare/', type='json', auth='none', cors='*')
    def scale_clear_tare(self):
        scale_thread.clear_tare()
        return True
